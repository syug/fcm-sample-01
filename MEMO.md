### MEMO

##### Firebase
- [firebase-messaging-sw.js](https://recipe.rakuten.co.jp/firebase-messaging-sw.js) for Rakuten Recipe
- As default, [Firebase.js](https://www.gstatic.com/firebasejs/4.1.1/firebase.js) will automatically register "/firebase-messaging-sw.js" at root.


##### webpack
- serve static mamifest.json by [kevlened/copy-webpack-plugin](https://github.com/kevlened/copy-webpack-plugin)

- add only 'app' chunk or exclude chunk for service worker
  [jantimon/html-webpack-plugin - Filtering chunks](https://github.com/jantimon/html-webpack-plugin#filtering-chunks)
- or, **without adding sw.js as webpack's entry point**
  - we can just copy it to dist folder by [kevlened/copy-webpack-plugin](https://github.com/kevlened/copy-webpack-plugin)
  - or, include as emittable file by file-loader, just like image assets
    - by using the same proceduer, we are able to get the path and register it manually
      [Progressive Web Apps with Webpack](https://michalzalecki.com/progressive-web-apps-with-webpack/)
        `import swURL from "file?name=sw.js!babel!./sw";`
  - here is a discussion about above 2 ways
   - [How to copy static files to build directory with Webpack?](https://stackoverflow.com/questions/27639005/how-to-copy-static-files-to-build-directory-with-webpack)
- or, try this [worker-loader](https://github.com/webpack-contrib/worker-loader)

- dev/build workflow with Service Workers
  - [Workerを駆使するためのプロジェクト構成 with webpack](http://qiita.com/_likr/items/d382dc120a942ba4c6fe)
  - plugins
    - simple one is -> [oliviertassinari/serviceworker-webpack-plugin](https://github.com/oliviertassinari/serviceworker-webpack-plugin)
    - complex and powerful, mainly for PWA -> [NekR/offline-plugin](https://github.com/NekR/offline-plugin)


- [webpack/webpack-dev-server](https://github.com/webpack/webpack-dev-server)
  - [DevServer - webpack](https://webpack.js.org/configuration/dev-server/)


##### Service Worker
- debug
  - [Service Worker 開発するときのデバッグ方法](http://qiita.com/tmtysk/items/f77e31d6e9380e1c94a2)
  - [chrome://serviceworker-internals/](chrome://serviceworker-internals/)


##### Stub
- [typicode/json-server](https://github.com/typicode/json-server) is introduced in [フロントエンド実装中に使えるモックサーバを爆速で準備する](http://qiita.com/ahaha0807_alg/items/a9b51ec238440ef341f1)


##### SSH
use -K option in order to store key in Keychain

`$ ssh-add -l`

`$ ssh-add -K /Users/SyugoSaito-MacBookAir/.ssh/syug-GitHub`

`$ ssh -T git@bitbucket.org`


in case of macOS Sierra
```
#!markdown
// .ssh/config
Host *
   AddKeysToAgent yes
   UseKeychain yes
   IdentityFile /Users/SyugoSaito-MacBookAir/.ssh/syug-Bitbucket
   IdentityFile /Users/SyugoSaito-MacBookAir/.ssh/syug-GitHub
```
This setting works on macOS Sierra 10.12.5（16F73）

- [ssh-agentを利用して、安全にSSH認証を行う](http://qiita.com/naoki_mochizuki/items/93ee2643a4c6ab0a20f5)
- [macOS Sierra のSSH接続で、秘密鍵へのパスを覚えてくれない問題](http://qiita.com/takumikkusu/items/3b18e475de02a91b37e8)
- [Saving SSH keys in macOS Sierra keychain](https://github.com/jirsbek/SSH-keys-in-macOS-Sierra-keychain)
