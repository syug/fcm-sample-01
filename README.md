# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
Learn how to implement Firebase Cloud Messaging.

### How do I get set up? ###
`$ npm install`

### Browser compativility

##### [Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API) / [ja](https://developer.mozilla.org/ja/docs/Web/API/Notifications_API)
Desktop

|機能|Chrome|Firefox (Gecko)|Internet Explorer|Opera|Safari (WebKit)|
|:-:|:-:|:-:|:-:|:-:|:-:|
|基本サポート|5 webkit[1]22|4.0 (2.0)moz[2]|22.0 (22.0)|未サポート|25|6[3]|
|Available in workers|?|41.0 (41.0)|?|?|?|
|Service worker additions|42.0|42.0 (42.0)[4]|?|?|?|

##### [Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API) / [ja](https://developer.mozilla.org/ja/docs/Web/API/Push_API)
Desktop

|機能|Chrome|Firefox (Gecko)|Internet Explorer|Opera|Safari (WebKit)|
|:-:|:-:|:-:|:-:|:-:|:-:|
|基本サポート|42.0|44.0 (44.0)[1][3]|未サポート[2]|?|?|
|PushEvent.data, PushMessageData|未サポート|44.0 (44.0)[3]|未サポート|未サポート|未サポート|


### APIs
##### Browser Native
- [Notifications API](https://developer.mozilla.org/en-US/docs/Web/API/Notifications_API) / [ja](https://developer.mozilla.org/ja/docs/Web/API/Notifications_API)
- [Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API) / [ja](https://developer.mozilla.org/ja/docs/Web/API/Push_API)
- [Web Workers API](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API) / [ja](https://developer.mozilla.org/ja/docs/Web/API/Web_Workers_API)
- [ServiceWorker API](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker_API) / [ja](https://developer.mozilla.org/ja/docs/Web/API/ServiceWorker_API)
  - [Using Service Workers](https://developer.mozilla.org/en-US/docs/Web/API/ServiceWorker_API/Using_Service_Workers) / [ja](https://developer.mozilla.org/ja/docs/Web/API/ServiceWorker_API/Using_Service_Workers)

### Note
- Notifications permission will be automatically blocked by chrome, if the user has dismissed the permission prompt several times. See https://www.chromestatus.com/features/6443143280984064 for more information.

### Other notes
[MEMO](MEMO.md)