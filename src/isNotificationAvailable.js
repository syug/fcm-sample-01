

export function isNotificationAvailable() {
  let b = true;

  // Check Notification
  if (!("Notification" in window)) {
    console.warn("このブラウザはシステム通知をサポートしていません");
    b = false;
  }
  // Check ServiceWorker
  if (!('serviceWorker' in navigator)) {
    b = false;
  }

  return b;
}