/**
 * index.js
 */

import manifest from './static/manifest';
console.log("manifest->");
console.log(manifest);

import { isNotificationAvailable } from './isNotificationAvailable';
console.log(`isNotificationAvailable()=${isNotificationAvailable()}`);

// Firebase
import * as firebase from 'firebase';
// Initialize Firebase
var config = {
  apiKey: "AIzaSyCToteBC_kD4bN8W-juVruwlHDyN7JQ_8A",
  authDomain: "dev-rex-push-notification.firebaseapp.com",
  databaseURL: "https://dev-rex-push-notification.firebaseio.com",
  projectId: "dev-rex-push-notification",
  storageBucket: "dev-rex-push-notification.appspot.com",
  messagingSenderId: "116718890387"
};
firebase.initializeApp(config);

const messaging = firebase.messaging();
messaging.requestPermission()
  .then(function () {
    console.log('Notification permission granted.');
    // TODO(developer): Retrieve an Instance ID token for use with FCM.
    // ...
  })
  .catch(function (err) {
    console.log('Unable to get permission to notify.', err);
  });

// Get Instance ID token. Initially this makes a network call, once retrieved
// subsequent calls to getToken will return from cache.
messaging.getToken()
  .then(function (currentToken) {
    if (currentToken) {
      sendTokenToServer(currentToken);
      updateUIForPushEnabled(currentToken);
    } else {
      // Show permission request.
      console.log('No Instance ID token available. Request permission to generate one.');
      // Show permission UI.
      updateUIForPushPermissionRequired();
      setTokenSentToServer(false);
    }
  })
  .catch(function (err) {
    console.log('An error occurred while retrieving token. ', err);
    showToken('Error retrieving Instance ID token. ', err);
    setTokenSentToServer(false);
  });

// Callback fired if Instance ID token is updated.
messaging.onTokenRefresh(function () {
  messaging.getToken()
    .then(function (refreshedToken) {
      console.log('Token refreshed.');
      // Indicate that the new Instance ID token has not yet been sent to the
      // app server.
      setTokenSentToServer(false);
      // Send Instance ID token to app server.
      sendTokenToServer(refreshedToken);
      // ...
    })
    .catch(function (err) {
      console.log('Unable to retrieve refreshed token ', err);
      showToken('Unable to retrieve refreshed token ', err);
    });
});

function showToken(currentToken) {
  // Show token in console and UI.
  var tokenElement = document.querySelector('body');
  tokenElement.textContent = currentToken;
}

// (pattern C) include firebase-messaging-sw.js by file-loader
// (pattern D) we can also register it manually.  -> we don't need to register when it comes to firebase-messagins-sw.js
//    https://michalzalecki.com/progressive-web-apps-with-webpack/
//    import swURL from "file?name=sw.js!babel!./sw";
// import swURL from "file-loader?name=firebase-messaging-sw.js!./firebase-messaging-sw.js"; // this line enable to emit firebase-messaging-sw.js
// console.log(`swURL=${swURL}`);

// // sw
// if ('serviceWorker' in navigator) {
//   navigator.serviceWorker.register('firebase-messaging-sw.js', {
//     scope: './'
//   }).then(function (registration) {
//     var serviceWorker;
//     if (registration.installing) {
//       serviceWorker = registration.installing;
//       document.querySelector('#kind').textContent = 'installing';
//     } else if (registration.waiting) {
//       serviceWorker = registration.waiting;
//       document.querySelector('#kind').textContent = 'waiting';
//     } else if (registration.active) {
//       serviceWorker = registration.active;
//       document.querySelector('#kind').textContent = 'active';
//     }
//     if (serviceWorker) {
//       // logState(serviceWorker.state);
//       serviceWorker.addEventListener('statechange', function (e) {
//         // logState(e.target.state);
//       });
//     }
//   }).catch(function (error) {
//     // Something went wrong during registration. The service-worker.js file
//     // might be unavailable or contain a syntax error.
//   });
// } else {
//   // The current browser doesn't support service workers.
// }

export class App {
  constructor() {

  }
}